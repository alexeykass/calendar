import React, { Component } from 'react';
import './Calendar.css';
import Headers from './Headers.js';
import Row from './Row.js';
// import Cell from './Cell.js';
import PropTypes from 'prop-types';

export default class Calendar extends Component {

    constructor(props){
	super(props);
	this.state = {
	    daysInMonthes: [31, this.props.year % 4 ? 28 : 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
	    divservs: []
	};
    }

    componentWillMount(){
	var st = this.state;
	var y = this.props.year;
	var m = this.props.month - 1;
	for(var i = 0; i < this.state.daysInMonthes[ m ]; i++){
	    st.divservs[i] = {
		date: i + 1,
		cN: (new Date(y,m,i+1)).getDay() === 0 ? 'sunday divserv' : 'othday'
	    };
	}
	this.setState(st);
	var ds = undefined;
	for (i = 0; i < this.props.divservs.length; i++){
	    if ((this.props.divservs[i].month === parseInt(this.props.month,10)) && (this.props.divservs[i].year === parseInt(this.props.year,10))) {
		ds = this.props.divservs[i];
	    }
	}
	st = this.state;
	if (ds){
	    for(i = 0; i < this.state.divservs.length; i++){
		for(var j = 0; j < ds.servs.length; j++){
		    if (this.state.divservs[i].date === ds.servs[j].date) {
			st.divservs[i].cN = ds.servs[j].cN;
		    }
		}
	    }
	    this.setState(st);
	}
    }
    
    render() {
	const year = this.props.year;
	const month = this.props.month - 1;
	const daysInCurMonth = this.state.daysInMonthes[ month ];
	var week = [];
	var weeks = [];
	var weekOfMonth = 0;
	for(var i = 1; i <= daysInCurMonth; i++){
	    var dayOfWeek = ( new Date( year, month, i ) ).getDay();
	    if ( dayOfWeek !== 0 ) {
		week[ dayOfWeek - 1 ] = this.state.divservs[i-1];
	    } else {
		week[ 6 ] = this.state.divservs[i-1];
		weeks[ weekOfMonth ] = week;
		week = [];
		weekOfMonth++;
	    };
	};
	if (week) {
	    weeks[ weekOfMonth ] = week;
	};
	for(i = 0; i < weeks.length; i++){
	    for(var j = 0; j < weeks[i].length; j++){
		weeks[i][j] = weeks[i][j] ? weeks[i][j] : { date: 0, cN: '' };
	    }
	}
	return (
	    <div id="calendar">
	      <table id="calendar">
		<Headers />
		{ weeks.map ( (week) => <Row week={week} /> ) }
	    </table>
	    </div>
	);
    }
}

// <p></p>
// <p align="center">
// <a href="http://вознесенско-георгиевский.рф/sample-page/bogosluzheniya">Подробнее</a><hr /></p>

Calendar.propTypes = {
    year: PropTypes.number,
    month: PropTypes.number,
    divservs: PropTypes.arrayOf(PropTypes.object)
};

Calendar.defaultProps = {
    year: (new Date()).getFullYear(),
    month: (new Date()).getMonth() + 1,
    divservs: []
};
