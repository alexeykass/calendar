import React, { Component } from 'react';
import './Cell.css';

export default class Cell extends Component {

    constructor(props){
	super(props);
	this.handleMouseOver = this.handleMouseOver.bind(this);
	this.handleMouseOut = this.handleMouseOut.bind(this);
    }

    handleMouseOver(e){
	this.props.onDateChange(e.target.id);
    }

    handleMouseOut(e){
	this.props.onDateChange((new Date()).getDate());
    }

    render(){
	const url = 'http://xn----ctbbdajacbnrcnfh1bcngd6bncl.xn--p1ai/sample-page/bogosluzheniya';
	if (this.props.day.cN.match(/divserv/)){
	    return (
		<td
		  id={this.props.day.date}
		  className={this.props.day.cN}>
		  <a href={url+'#d'+this.props.day.date}>
		    {this.props.day.date ? this.props.day.date : ''}
		  </a>
		</td>
	    );
	} else {
	    return (
		<td
		  id={this.props.day.date}
		  className={this.props.day.cN}>
		  {this.props.day.date ? this.props.day.date : ''}
		</td>
	    );
	}
    }
}
