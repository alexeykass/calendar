import React, { Component } from 'react';
import './Row.css';
import Cell from './Cell.js';

export default class Row extends Component {

    render() {
	return (
	    <thead>
		<tr>
		  {this.props.week.map( (day) => <Cell day={day} /> )}
		</tr>
	    </thead>
	);
    }
}
