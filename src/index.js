import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Calendar from './Calendar';
import registerServiceWorker from './registerServiceWorker';

var DIVSERVS = [
    {
	month: 9,
	servs: [
	    { date: 11, cN: 'othday divserv' },
	    { date: 21, cN: 'othday divserv Theotokos' },
	    { date: 27, cN: 'othday divserv Cross' }
	]
    },
    {
	year: 2017,
	month: 10,
	servs: [
	    { date: 6, cN: 'othday divserv' },
	    { date: 7, cN: 'othday divserv' },
	    { date: 8, cN: 'sunday divserv' },
	    { date: 13, cN: 'othday divserv' },
	    { date: 14, cN: 'othday divserv Theotokos'},
	    { date: 15, cN: 'sunday divserv' },
	    { date: 20, cN: 'othday divserv' },
	    { date: 21, cN: 'othday divserv' },
	    { date: 22, cN: 'sunday divserv' },
	    { date: 27, cN: 'othday divserv' },
	    { date: 28, cN: 'othday divserv' },
	    { date: 29, cN: 'sunday divserv' },
	]
    },
    {
	year: 2017,
	month: 11,
	servs: [
	    { date: 3, cN: 'othday divserv' },
	    { date: 4, cN: 'othday divserv' },
	    { date: 5, cN: 'sunday divserv' },
	    { date: 10, cN: 'othday divserv' },
	    { date: 11, cN: 'othday divserv'},
	    { date: 12, cN: 'sunday divserv' },
	    { date: 17, cN: 'othday divserv' },
	    { date: 18, cN: 'othday divserv' },
	    { date: 19, cN: 'sunday divserv' },
	    { date: 20, cN: 'othday divserv' },
	    { date: 21, cN: 'othday divserv Angels' },
	    { date: 24, cN: 'othday divserv' },
	    { date: 25, cN: 'othday divserv' },
	    { date: 26, cN: 'sunday divserv' },
	]
    }
];

ReactDOM.render(<Calendar month="11" divservs={DIVSERVS} />, document.getElementById('root'));
registerServiceWorker();
