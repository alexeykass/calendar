import React, { Component } from 'react';
import './Headers.css';
import Header from './Header.js';

export default class Headers extends Component{

    render(){
	const days = ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
	return (
	    <thead>
	      <tr>
		{ days.map( (name) => <Header name={name} /> ) }
	    </tr>
		</thead>
	    );
    }
    
}
