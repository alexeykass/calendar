var http = require("http");
var url = require("url");


function onRequest(request, response) {
    console.log("Request received.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    var out = url.parse(request.url).pathname;
    response.write(out);
    console.log(out);
    response.end();
}

http.createServer(onRequest).listen(8888);

console.log("Server has started.");
